package com.eperezaco.core.WebServices.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Personaje : Serializable {
    @SerializedName("id")
    var id: Int = 0

    @SerializedName("name")
    lateinit var name: String

    @SerializedName("status")
    lateinit var status: String

    @SerializedName("species")
    lateinit var species: String

    @SerializedName("type")
    lateinit var type: String

    @SerializedName("gender")
    lateinit var gender: String

    @SerializedName("origin")
    lateinit var origin: Origin

    @SerializedName("location")
    lateinit var location: Location

    @SerializedName("episode")
    lateinit var episode: ArrayList<String>

    @SerializedName("image")
    lateinit var image: String
}

class Origin : Serializable {

    @SerializedName("name")
    lateinit var name: String

    @SerializedName("url")
    lateinit var url: String

}

class Location : Serializable {


    @SerializedName("name")
    lateinit var name: String

    @SerializedName("url")
    lateinit var url: String

}

class Episodio : Serializable {
    @SerializedName("url")
    lateinit var url: String

    @SerializedName("created")
    lateinit var created: String
}