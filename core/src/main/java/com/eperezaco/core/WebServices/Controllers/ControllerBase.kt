package com.eperezaco.core.WebServices.Controllers

import android.content.ContentValues.TAG
import android.content.Context
import android.util.Log
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.security.cert.CertificateException
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

open class ControllerBase(ctx: Context) {
    private lateinit var logging: HttpLoggingInterceptor
    private var urlBase = "https://rickandmortyapi.com/"
    var retrofit: Retrofit = getClientRetrofit(urlBase, false);
    var context: Context = ctx


    fun getClientRetrofit(urlBase: String, acceptNulls: Boolean): Retrofit {

        Log.d(TAG, "getClientRetrofit:$urlBase");

        return Retrofit.Builder()
            .addConverterFactory(if (acceptNulls) GsonConverterFactory.create(GsonBuilder().serializeNulls().create()) else GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(getUrlBaseFormat(urlBase))
            .client(getClientHttp().build())
            .build();
    }

    fun getUrlBaseFormat(urlBase: String): String? {
        return if (urlBase.endsWith("/")) urlBase else "$urlBase/"
    }

    fun getClientHttp(): OkHttpClient.Builder {
        val httpClient = OkHttpClient.Builder()
        logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(logging)
        httpClient.addNetworkInterceptor(StethoInterceptor())

        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
            @Throws(CertificateException::class)
            override fun checkClientTrusted(
                chain: Array<java.security.cert.X509Certificate>,
                authType: String
            ) {
            }

            @Throws(CertificateException::class)
            override fun checkServerTrusted(
                chain: Array<java.security.cert.X509Certificate>,
                authType: String
            ) {
            }

            override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                return arrayOf()
            }
        })
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, java.security.SecureRandom())
        val sslSocketFactory = sslContext.socketFactory
        httpClient.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
        httpClient.hostnameVerifier { hostname, session -> true }
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        return httpClient
    }

}