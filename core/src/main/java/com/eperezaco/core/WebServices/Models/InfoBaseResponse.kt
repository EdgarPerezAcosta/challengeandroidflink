package com.eperezaco.core.WebServices.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class InfoBaseResponse : Serializable {

    @SerializedName("count")
    var count: Int = 0

    @SerializedName("pages")
    var pages: Int = 0

    @SerializedName("next")
    lateinit var next: String

    @SerializedName("prev")
    lateinit var prev: String
}