package com.eperezaco.core.WebServices.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PersonajesResponse : Serializable {

    @SerializedName("info")
    lateinit var info: InfoBaseResponse

    @SerializedName("results")
    lateinit var results: ArrayList<Personaje>
}