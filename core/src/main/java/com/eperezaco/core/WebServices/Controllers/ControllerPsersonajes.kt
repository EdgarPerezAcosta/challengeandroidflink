package com.eperezaco.core.WebServices.Controllers

import android.content.Context
import com.eperezaco.core.WebServices.Models.PersonajesResponse
import com.eperezaco.core.WebServices.Services.ApiPersonajesService
import retrofit2.Call

class ControllerPsersonajes(val ctx: Context) : ControllerBase(ctx),
    ApiPersonajesService {

    val serviceInterface: ApiPersonajesService = retrofit.create(ApiPersonajesService::class.java)

    override fun getPersonajesList(): Call<PersonajesResponse> {
        return serviceInterface.getPersonajesList()
    }

    override fun getPersonajesByPage(page:Int): Call<PersonajesResponse> {
        return serviceInterface.getPersonajesByPage(page)
    }

}