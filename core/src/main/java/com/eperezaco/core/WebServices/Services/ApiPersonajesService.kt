package com.eperezaco.core.WebServices.Services

import com.eperezaco.core.WebServices.Models.PersonajesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiPersonajesService {

    @GET("api/character/")
    fun getPersonajesList(): Call<PersonajesResponse>



    @GET("api/character/")
    fun getPersonajesByPage(@Query("page") page: Int): Call<PersonajesResponse>
}