package com.eperezaco.core.WebServices.Utils

import android.content.Context

class Utils {
    companion object {
        fun getWidthDisplay(context: Context): Int {
            return context.resources.displayMetrics.widthPixels
        }
        fun convertDipToPixel(dip: Int, context: Context): Int {
            val scale = context.resources.displayMetrics.density
            return (dip * scale + 0.5f).toInt()
        }

    }
}