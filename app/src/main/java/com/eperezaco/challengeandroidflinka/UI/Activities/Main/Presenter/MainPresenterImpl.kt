package com.eperezaco.challengeandroidflinka.UI.Activities.Main.Presenter

import android.content.Context
import android.util.Log
import com.eperezaco.challengeandroidflinka.Base.BaseApplication
import com.eperezaco.challengeandroidflinka.R
import com.eperezaco.core.WebServices.Models.InfoBaseResponse
import com.eperezaco.core.WebServices.Models.Personaje
import com.eperezaco.core.WebServices.Models.PersonajesResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainPresenterImpl : MainPresenter {

    private lateinit var view: viewMain

    interface viewMain {
        fun setData(
            info: InfoBaseResponse,
            results: ArrayList<Personaje>
        )

        fun setError(message: String)
        fun setDataUpdate(info: InfoBaseResponse, results: ArrayList<Personaje>)
    }

    override fun setView(view: viewMain) {
        this.view = view
    }

    override fun getPersonajes(context: Context) {
        BaseApplication.getIntance()!!.controllerPersonajes.getPersonajesList()
            .enqueue(object : Callback<PersonajesResponse> {
                override fun onFailure(call: Call<PersonajesResponse>, t: Throwable) {
                    Log.e("ERORPERSONAJES", t.toString())
                    view.setError(context.resources.getString(R.string.server_error))
                }

                override fun onResponse(
                    call: Call<PersonajesResponse>,
                    response: Response<PersonajesResponse>
                ) {
                    var response = response.body()!!
                    view.setData(response.info, response.results)
                }
            })
    }


    override fun getPersonajesByPage(context: Context, page: Int) {
        BaseApplication.getIntance()!!.controllerPersonajes.getPersonajesByPage(page)
            .enqueue(object : Callback<PersonajesResponse> {
                override fun onFailure(call: Call<PersonajesResponse>, t: Throwable) {
                    view.setError(context.resources.getString(R.string.server_error))
                }

                override fun onResponse(
                    call: Call<PersonajesResponse>,
                    response: Response<PersonajesResponse>
                ) {
                    var response = response.body()!!
                    view.setDataUpdate(response.info, response.results)
                    view.setError("Información actualizada, pag : $page")
                }
            })
    }
}