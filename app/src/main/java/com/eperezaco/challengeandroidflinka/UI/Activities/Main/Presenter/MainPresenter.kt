package com.eperezaco.challengeandroidflinka.UI.Activities.Main.Presenter

import android.content.Context

interface MainPresenter {
    fun setView(view: MainPresenterImpl.viewMain)
    fun getPersonajes(context: Context)
    fun getPersonajesByPage(context: Context, page: Int)
}