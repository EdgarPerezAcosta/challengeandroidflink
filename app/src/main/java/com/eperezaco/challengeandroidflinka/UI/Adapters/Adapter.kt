package com.eperezaco.challengeandroidflinka.UI.Adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.eperezaco.challengeandroidflinka.R
import com.eperezaco.core.WebServices.Models.Personaje
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*
import kotlin.collections.ArrayList


class Adapter(
    private val context: Context,
    var list: ArrayList<Personaje>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    private var oldList = list
    private val FOOTER_VIEW = 1
    lateinit var listener: listenerPersonajeOnClick

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var itemView: View
        if (viewType == FOOTER_VIEW) {
            itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_footer_personaje, parent, false)
            return FooterViewHolder(itemView)
        }
        itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_personaje, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        if (list == null) {
            return 0
        }
        if (list.size == 0) {
            return 1
        }
        return list.size + 1
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        try {
            if (holder is ViewHolder) {
                var item = list[position]
                Picasso.with(context).load(item.image).into((holder as ViewHolder).civ)
                (holder as ViewHolder).tvTitle.text = item.name
                (holder as ViewHolder).tvsecond.text = item.species
                (holder as ViewHolder).tvDescription.text = item.status
                (holder as ViewHolder).clItem.setOnClickListener {
                    listener.selectPersonaje(item, holder.civ)
                }
            } else if (holder is FooterViewHolder) {
                if (list.size != oldList.size) {
                    (holder as FooterViewHolder).clItem.visibility = View.GONE

                }
            }
        } catch (e: Exception) {
            Log.e("Exception", e.toString())
        }


    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == list.size) { // This is where we'll add footer.
            FOOTER_VIEW
        } else {
            super.getItemViewType(position)
        }
    }

    public class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvTitle: TextView = view.findViewById(R.id.tvTitle)
        var tvsecond: TextView = view.findViewById(R.id.tvsecond)
        var tvDescription: TextView = view.findViewById(R.id.tvDescription)
        var civ: CircleImageView = view.findViewById(R.id.civ)
        var clItem: ConstraintLayout = view.findViewById(R.id.clItem)
    }

    public class FooterViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var clItem: ConstraintLayout = view.findViewById(R.id.clItem)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): Filter.FilterResults {
                val filterString = constraint.toString().toLowerCase(Locale.ENGLISH)
                val results = FilterResults()
                val nlist = ArrayList<Personaje>()
                for (item in oldList) {
                    val strigForFilter = item.name + item.origin + item.species + item.status
                    if (strigForFilter.toLowerCase(Locale.ENGLISH).startsWith(filterString) ||
                        strigForFilter.toLowerCase(Locale.ENGLISH).contains(filterString)
                        || strigForFilter.toLowerCase(Locale.ENGLISH).endsWith(filterString)
                    ) {
                        nlist.add(item)
                    }
                }
                results.values = if (nlist.size > 0) nlist else java.util.ArrayList<Any>()
                results.count = if (nlist.size > 0) nlist.size else oldList.size
                return results
            }

            override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {
                val filteredData = results.values as ArrayList<Personaje>
                list = filteredData
                notifyDataSetChanged()
            }
        }
    }

    fun update(lst: ArrayList<Personaje>) {
        oldList.addAll(lst)
        list = oldList
        notifyDataSetChanged()
    }

    interface listenerPersonajeOnClick {
        fun selectPersonaje(
            item: Personaje,
            civ: CircleImageView
        )
    }

    fun setOnItemClickListener(listener: listenerPersonajeOnClick) {
        this.listener = listener
    }
}