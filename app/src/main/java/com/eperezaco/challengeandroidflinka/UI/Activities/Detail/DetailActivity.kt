package com.eperezaco.challengeandroidflinka.UI.Activities.Detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import com.eperezaco.challengeandroidflinka.R
import com.eperezaco.challengeandroidflinka.UI.Activities.Detail.Presenter.DetailPresenter
import com.eperezaco.challengeandroidflinka.UI.Activities.Detail.Presenter.DetailPresenterImpl
import com.eperezaco.challengeandroidflinka.UI.Adapters.Adapter
import com.eperezaco.core.WebServices.Models.Personaje
import com.google.android.material.appbar.AppBarLayout
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlin.math.roundToInt

class DetailActivity : AppCompatActivity(), DetailPresenterImpl.viewDetail {

    private lateinit var presenter: DetailPresenter
    private lateinit var tvName: AppCompatTextView
    private lateinit var tvEspecie: AppCompatTextView
    private lateinit var tvEstatus: AppCompatTextView
    private lateinit var tvType: AppCompatTextView
    private lateinit var tvGender: AppCompatTextView
    private lateinit var tvOrigen: AppCompatTextView
    private lateinit var ivProfile: CircleImageView
    private lateinit var app_bar_layout: AppBarLayout
    private lateinit var toolbar:Toolbar
    private var avatarAnimateStartPointY: Float = 0F
    private var avatarCollapseAnimationChangeWeight: Float = 0F

    private var verticalToolbarAvatarMargin = 0F
    private var EXPAND_AVATAR_SIZE: Float = 0F
    private var COLLAPSE_IMAGE_SIZE: Float = 0F
    private var horizontalToolbarAvatarMargin: Float = 0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        initView()
        setPresenter()
    }

    private fun setPresenter() {
        presenter = DetailPresenterImpl()
        presenter.setView(this)
        presenter.setArguments(intent)
    }

    fun initView() {
        ivProfile = findViewById(R.id.ivProfile)
        tvName = findViewById(R.id.tvName)
        tvEspecie = findViewById(R.id.tvEspecie)
        tvEstatus = findViewById(R.id.tvEstatus)
        tvType = findViewById(R.id.tvType)
        tvGender = findViewById(R.id.tvGender)
        tvOrigen = findViewById(R.id.tvOrigen)
        app_bar_layout = findViewById(R.id.app_bar_layout)
        toolbar=findViewById(R.id.anim_toolbar)

    }

    override fun setData(info: Personaje) {
        Picasso.with(applicationContext).load(info.image).into(ivProfile)
        tvName.text = info.name
        tvEspecie.text = info.species
        tvEstatus.text = info.status
        tvType.text = info.type
        tvGender.text = info.gender
        tvOrigen.text = info.origin.name

    }
}
