package com.eperezaco.challengeandroidflinka.UI.Activities.Main

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityOptionsCompat

import androidx.core.view.MenuItemCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eperezaco.challengeandroidflinka.R
import com.eperezaco.challengeandroidflinka.UI.Activities.Detail.DetailActivity
import com.eperezaco.challengeandroidflinka.UI.Adapters.Adapter
import com.eperezaco.challengeandroidflinka.UI.Activities.Main.Presenter.MainPresenter
import com.eperezaco.challengeandroidflinka.UI.Activities.Main.Presenter.MainPresenterImpl
import com.eperezaco.core.Utils.Constants
import com.eperezaco.core.WebServices.Models.InfoBaseResponse
import com.eperezaco.core.WebServices.Models.Personaje
import com.eperezaco.core.WebServices.Utils.Utils
import com.google.android.material.snackbar.Snackbar
import de.hdodenhof.circleimageview.CircleImageView
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter


class MainActivity : AppCompatActivity(), MainPresenterImpl.viewMain {

    private lateinit var searchView: SearchView
    private lateinit var infoLocal: InfoBaseResponse
    private lateinit var adapter: Adapter
    private var page: Int = 1
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var isLoading = false
    private lateinit var rv: RecyclerView
    private lateinit var pbUploading: ProgressBar
    private lateinit var clMain: ConstraintLayout
    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        setListeners()
        setPresenter()

    }

    private fun setListeners() {
        rv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (!isLoading) {
                    if (searchView.isIconified) {
                        if (linearLayoutManager.findLastVisibleItemPosition() == linearLayoutManager.itemCount - 1) {
                            isLoading = true
                            page++
                            presenter.getPersonajesByPage(applicationContext, page)
                        }
                    }
                }
            }

        })
    }

    private fun initView() {
        clMain = findViewById(R.id.clMain)
        rv = findViewById(R.id.rv)
        pbUploading = findViewById(R.id.pbUploading)
    }

    private fun setPresenter() {
        pbUploading.visibility = View.VISIBLE
        presenter = MainPresenterImpl()
        presenter.setView(this)
        presenter.getPersonajes(applicationContext)
    }

    override fun setData(info: InfoBaseResponse, results: ArrayList<Personaje>) {
        this.infoLocal = info
        pbUploading.visibility = View.GONE
        adapter = Adapter(applicationContext, results)
        linearLayoutManager = LinearLayoutManager(applicationContext)
        rv.layoutManager = linearLayoutManager
        val alphaAdpt = AlphaInAnimationAdapter(adapter)
        alphaAdpt.setDuration(800)
        val animation =
            AnimationUtils.loadLayoutAnimation(
                applicationContext,
                R.anim.layout_animation_fall_down
            )

        adapter.listener = object : Adapter.listenerPersonajeOnClick {
            override fun selectPersonaje(
                item: Personaje,
                civ: CircleImageView
            ) {
                val intent = Intent(applicationContext, DetailActivity::class.java)
                intent.putExtra(Constants.DATA_SEND, item)
                if (Build.VERSION.SDK_INT < 21) {
                    startActivity(
                        intent,
                        ActivityOptionsCompat.makeSceneTransitionAnimation(
                            Activity(),
                            civ,
                            civ.transitionName
                        ).toBundle()
                    )
                } else {
                    startActivity(intent)
                }
            }
        }
        rv.layoutAnimation = animation
        rv.adapter = alphaAdpt
        rv.scheduleLayoutAnimation()
    }

    private fun getColumns(): Int {
        return Utils.getWidthDisplay(applicationContext) / Utils.convertDipToPixel(
            170,
            applicationContext
        )
    }

    override fun setError(message: String) {
        pbUploading.visibility = View.GONE
        Snackbar.make(clMain, message, Snackbar.LENGTH_LONG).show()
    }

    override fun setDataUpdate(info: InfoBaseResponse, results: ArrayList<Personaje>) {
        adapter.update(results)
        isLoading = false
        searchView.setQuery("", false)
        searchView.isIconified = true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        val searchItem: MenuItem = menu.findItem(R.id.action_search)
        searchView = MenuItemCompat.getActionView(searchItem) as SearchView
        searchView.queryHint = getText(R.string.action_search)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchView.setQuery("", false)
                searchView.isIconified = true

                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {

                adapter.filter.filter(newText)
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }


}
