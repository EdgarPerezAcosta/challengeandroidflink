package com.eperezaco.challengeandroidflinka.UI.Activities.Detail.Presenter

import android.content.Intent
import com.eperezaco.core.Utils.Constants
import com.eperezaco.core.WebServices.Models.Personaje

class DetailPresenterImpl : DetailPresenter {

    private lateinit var view: viewDetail

    interface viewDetail {
        fun setData(info: Personaje)
    }

    override fun setView(view: viewDetail) {
        this.view = view
    }


    override fun setArguments(intent: Intent) {
        var data = intent.getSerializableExtra(Constants.DATA_SEND) as Personaje
        view.setData(data)
    }
}