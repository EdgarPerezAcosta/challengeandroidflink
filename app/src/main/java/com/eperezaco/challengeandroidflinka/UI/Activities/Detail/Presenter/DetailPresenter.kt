package com.eperezaco.challengeandroidflinka.UI.Activities.Detail.Presenter

import android.content.Intent

interface DetailPresenter {
    fun setView(view: DetailPresenterImpl.viewDetail)
    fun setArguments(intent: Intent)
}