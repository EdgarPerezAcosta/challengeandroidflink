package com.eperezaco.challengeandroidflinka.Base

import android.app.Application
import android.content.Context
import com.eperezaco.core.WebServices.Controllers.ControllerPsersonajes
import com.facebook.stetho.Stetho

class BaseApplication : Application() {

    val TAG: String = BaseApplication::class.java.simpleName
    private lateinit var context: Context
    lateinit var controllerPersonajes: ControllerPsersonajes

    override fun onCreate() {
        super.onCreate()
        initApplication()
    }

    companion object {
        var instance: BaseApplication? = null
        fun getIntance(): BaseApplication? {
            if (instance == null) {
                instance = BaseApplication()
                instance!!.initApplication()
            }
            return instance
        }
    }

    private fun initApplication() {
        instance = this
        context = applicationContext
        controllerPersonajes = ControllerPsersonajes(context)
        Stetho.initializeWithDefaults(this)
    }
}